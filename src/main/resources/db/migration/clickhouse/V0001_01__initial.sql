CREATE TABLE bpmlight.model (
    created DateTime64(3),
    deploymentKey String,
    fileName String
) engine = ReplacingMergeTree PRIMARY KEY deploymentKey
ORDER BY (deploymentKey);

CREATE TABLE bpmlight.activity (
    created DateTime64(3),
    processInstanceId String,
    activityId String,
    activityInstanceId String,
    lifecycleType String,
    endDate Nullable(DateTime64(3))
) engine = MergeTree PRIMARY KEY activityInstanceId
ORDER BY (activityInstanceId,created);

CREATE TABLE bpmlight.deployment(
    created DateTime64(3),
    deploymentId String,
    xml String,
    fileName String,
    deploymentKey String
) engine = MergeTree PRIMARY KEY deploymentId
ORDER BY (deploymentId,created);

CREATE TABLE bpmlight.process(
    created DateTime64(3),
    processDefinitionKey String,
    processDefinitionId String,
    businessKey String,
    processInstanceId String,
    superProcessInstanceId String,
    lifecycleType String,
    endDate Nullable(DateTime64(3))
) engine = MergeTree PRIMARY KEY processInstanceId
ORDER BY (processInstanceId,created);

CREATE TABLE bpmlight.variable(
    created DateTime64(3),
    processInstanceId String,
    variableName String,
    textValue String,
    serializerName String
) engine = MergeTree
ORDER BY (created);