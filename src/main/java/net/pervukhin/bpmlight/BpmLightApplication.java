package net.pervukhin.bpmlight;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BpmLightApplication {

    public static void main(String[] args) {
        SpringApplication.run(BpmLightApplication.class, args);
    }

}
