package net.pervukhin.bpmlight.exporter.clickhouse.repository;

import net.pervukhin.bpmlight.dto.PaginationContainerDTO;
import net.pervukhin.bpmlight.exporter.clickhouse.ClickhouseConnectionService;
import net.pervukhin.bpmlight.model.Model;
import net.pervukhin.bpmlight.repository.ModelRepositoryInt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;

@Service("modelRepository")
@ConditionalOnProperty(prefix = "exporter", name = "type", havingValue = "clickhouse")
public class ModelRepository implements ModelRepositoryInt {
    @Autowired
    private ClickhouseConnectionService databaseConnectionService;

    private final ModelMapper modelMapper = new ModelMapper();

    @Override
    public PaginationContainerDTO<Model> getList(String maxResults, String firstResults) {
        return new PaginationContainerDTO<>(
                databaseConnectionService.getJdbcTemplate().queryForObject(
                        "SELECT COUNT() FROM bpmlight.model ", Integer.class),
                databaseConnectionService.getJdbcTemplate().query(
                        "SELECT * FROM bpmlight.model order by created  limit ? offset ?",
                        modelMapper, Integer.parseInt(maxResults), Integer.parseInt(firstResults)
                )
        );
    }

    class ModelMapper implements RowMapper<Model> {
        @Override
        public Model mapRow(ResultSet rs, int rowNum) throws SQLException {
            final Model model = new Model();
            model.setDate(rs.getTimestamp("created"));
            model.setDeploymentKey(rs.getString("deploymentKey"));
            model.setFileName(rs.getString("fileName"));
            return model;
        }
    }
}
