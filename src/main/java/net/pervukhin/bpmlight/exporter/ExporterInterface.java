package net.pervukhin.bpmlight.exporter;

import net.pervukhin.bpmlight.model.ExportRecord;
import net.pervukhin.bpmlight.model.ExportRecordType;

import java.util.concurrent.BlockingQueue;

public interface ExporterInterface {
    void init();
    void export(ExportRecordType exportRecordType, BlockingQueue<ExportRecord> records);
    void destroy();
}
