package net.pervukhin.bpmlight.exporter.elastic.repository;

import net.pervukhin.bpmlight.dto.PaginationContainerDTO;
import net.pervukhin.bpmlight.exporter.elastic.dto.ElasticGetOne;
import net.pervukhin.bpmlight.exporter.elastic.dto.ElasticResult;
import net.pervukhin.bpmlight.exporter.elastic.dto.HitsContainer;
import net.pervukhin.bpmlight.model.Deployment;
import net.pervukhin.bpmlight.repository.DeploymentRepositoryInt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.stream.Collectors;

@Service("deploymentRepository")
@ConditionalOnProperty(prefix = "exporter", name = "type", havingValue = "elastic")
public class DeploymentRepository implements DeploymentRepositoryInt {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${rest.deployment.listUrl}")
    private String deploymentListUrl;

    @Value("${rest.deployment.getOne}")
    private String deploymentGetOne;

    public PaginationContainerDTO<Deployment> getList(String deploymentKey, String maxResults, String firstResults) {
        ElasticResult<Deployment> result = restTemplate.exchange(
                deploymentListUrl
                        .replace("{deploymentKey}", deploymentKey)
                        .replace("{size}", maxResults)
                        .replace("{from}", firstResults),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<ElasticResult<Deployment>>() {}).getBody();

        return result != null
            ? new PaginationContainerDTO<>(
                result.getHits().getTotal().getValue(),
                result.getHits().getHits().stream().map(HitsContainer::getSource)
                        .collect(Collectors.toList()))
            : null;
    }

    public Deployment get(String deploymentId) {
        ElasticGetOne<Deployment> result = restTemplate.exchange(
                deploymentGetOne
                        .replace("{id}", deploymentId),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<ElasticGetOne<Deployment>>() {}).getBody();
        return result != null ? result.getSource() : null;
    }
}
