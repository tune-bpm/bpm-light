package net.pervukhin.bpmlight.exporter.elastic;

import static net.pervukhin.bpmlight.model.LifecycleType.ENDED;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import net.pervukhin.bpmlight.configuration.ObjectMapperConfiguration;
import net.pervukhin.bpmlight.exporter.ExporterInterface;
import net.pervukhin.bpmlight.exporter.elastic.dto.end.ActivityEnd;
import net.pervukhin.bpmlight.exporter.elastic.dto.end.ProcessEnd;
import net.pervukhin.bpmlight.model.Activity;
import net.pervukhin.bpmlight.model.Deployment;
import net.pervukhin.bpmlight.model.ExportRecord;
import net.pervukhin.bpmlight.model.ExportRecordType;
import net.pervukhin.bpmlight.model.Model;
import net.pervukhin.bpmlight.model.Process;
import net.pervukhin.bpmlight.util.IoUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;

@Service("exporter")
@ConditionalOnProperty(prefix = "exporter", name = "type", havingValue = "elastic")
public class ElasticExporter implements ExporterInterface {
    private static final Logger logger = LoggerFactory.getLogger(ElasticExporter.class);

    private ObjectMapper objectMapper;
    private HttpClient httpClient;

    @Value("${exporter.url}")
    private String url;

    @Override
    public void init() {
        this.objectMapper = new ObjectMapperConfiguration().objectMapper();
        this.objectMapper.disable(SerializationFeature.INDENT_OUTPUT);
        this.httpClient = HttpClient.newBuilder().build();
        checkIndexes();
    }

    @Override
    public void export(ExportRecordType exportRecordType, BlockingQueue<ExportRecord> records) {
        final List<String> items = new ArrayList<>();

        while (!records.isEmpty()) {
            final ExportRecord recordElement = records.poll();
            items.add(serializeItem(exportRecordType, recordElement));
        }
        if (! items.isEmpty()) {
            final String bulkString = String.join("", items);
            exportToElastic(exportRecordType.name().toLowerCase(), bulkString);
        }
    }

    public void destroy() {
        // HTTP Client завершится автоматически
    }

    private String serializeItem(ExportRecordType exportRecordType, ExportRecord object) {
        try {
            switch (exportRecordType) {
                case PROCESS:
                    final Process process = (Process) object;
                    if (ENDED.equals(process.getLifecycleType())) {
                        final ProcessEnd processEnd = new ProcessEnd(
                                process.getProcessInstanceId(),
                                process.getEndDate()
                        );
                        return "{\"update\": " + "{\"_id\":\"" + process.getProcessInstanceId() + "\"}" + "}\n"
                             + objectMapper.writeValueAsString(processEnd) +"\n";
                    } else {
                        return "{\"index\": " + "{\"_id\":\"" + process.getProcessInstanceId() + "\"}" + "}\n"
                                + objectMapper.writeValueAsString(process) +"\n";
                    }
                case ACTIVITY:
                    final Activity activity = (Activity) object;
                    if (ENDED.equals(activity.getLifecycleType())) {
                        final ActivityEnd activityEnd = new ActivityEnd(
                                activity.getActivityInstanceId(),
                                activity.getEndDate()
                        );
                        return "{\"update\": " + "{\"_id\":\"" + activity.getActivityInstanceId() + "\"}" + "}\n"
                                + objectMapper.writeValueAsString(activityEnd) +"\n";
                    } else {
                        return "{\"index\": " + "{\"_id\":\"" + activity.getActivityInstanceId() + "\"}" + "}\n"
                                + objectMapper.writeValueAsString(activity) +"\n";
                    }
                case VARIABLE:
                    return "{\"index\": " + "{}" + "}\n"
                            + objectMapper.writeValueAsString(object) +"\n";
                case DEPLOYMENT:
                    return "{\"index\": " + "{\"_id\":\"" + ((Deployment) object).getDeploymentId() + "\"}" + "}\n"
                            + objectMapper.writeValueAsString(object) +"\n";
                case MODEL:
                    return "{\"index\": " + "{\"_id\":\"" + ((Model) object).getDeploymentKey() + "\"}" + "}\n"
                            + objectMapper.writeValueAsString(object) +"\n";
                default:
                    throw new RuntimeException("Неподдерживаемый тип записи");
            }
        } catch (JsonProcessingException ex) {
            logger.error("Error serializing", ex);
            return "";
        }
    }

    private void checkIndexes() {
        Arrays.stream(ExportRecordType.values()).forEach(item -> {
            try {
                HttpRequest request = HttpRequest.newBuilder()
                        .uri(new URI(url + item.name().toLowerCase() ))
                        .setHeader("content-type", "application/json")
                        .GET()
                        .build();

                HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
                if (response.statusCode() == 404) {
                    HttpRequest createRequest = HttpRequest.newBuilder()
                            .uri(new URI(url + item.name().toLowerCase() ))
                            .setHeader("content-type", "application/json")
                            .PUT(HttpRequest.BodyPublishers.ofString(
                                    IoUtil.getResourceFileAsString("elastic/migration/put-"
                                            + item.name().toLowerCase() + ".json"
                                    )))
                           .build();
                    HttpResponse<String> createResponse =
                            httpClient.send(createRequest, HttpResponse.BodyHandlers.ofString());
                    if (createResponse != null && createResponse.statusCode() == 200) {
                        logger.info("Index " + item.name().toLowerCase() + " created");
                    } else {
                        logger.error("Error creating index " + item.name() + " " + createResponse.body());
                    }
                }
            } catch (Exception ex) {
                logger.error("Error exporting to Elastic", ex);
            }
        });
    }

    private void exportToElastic(String objectName, String exportString) {
        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(new URI(url + objectName + "/_bulk"))
                    .setHeader("content-type", "application/json")
                    .POST(HttpRequest.BodyPublishers.ofString(exportString))
                    .build();

            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            logger.debug(response != null ? response.body() : "Null result");
        } catch (Exception ex) {
            logger.error("Error exporting to Elastic", ex);
            Thread.currentThread().interrupt();
        }
    }
}
