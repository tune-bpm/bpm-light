package net.pervukhin.bpmlight.exporter.elastic.dto;

import net.pervukhin.bpmlight.model.ExportRecord;

import java.util.List;

public class HitsResult<T extends ExportRecord> {
    private TotalValue total;
    private List<HitsContainer<T>> hits;

    public TotalValue getTotal() {
        return total;
    }

    public void setTotal(TotalValue total) {
        this.total = total;
    }

    public List<HitsContainer<T>> getHits() {
        return hits;
    }

    public void setHits(List<HitsContainer<T>> hits) {
        this.hits = hits;
    }
}
