package net.pervukhin.bpmlight.exporter.elastic.dto.aggregation;

import com.fasterxml.jackson.annotation.JsonProperty;
import net.pervukhin.bpmlight.model.ExportRecord;

public class PreFilter <T extends ExportRecord> {
    @JsonProperty("doc_count")
    private Integer docCount;

    private Aggregated<T> aggregated;

    public Integer getDocCount() {
        return docCount;
    }

    public void setDocCount(Integer docCount) {
        this.docCount = docCount;
    }

    public Aggregated<T> getAggregated() {
        return aggregated;
    }

    public void setAggregated(Aggregated<T> aggregated) {
        this.aggregated = aggregated;
    }
}
