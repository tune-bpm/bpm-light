package net.pervukhin.bpmlight.exporter.elastic.dto.end;

import net.pervukhin.bpmlight.model.LifecycleType;

import java.util.Date;

public class ActivityEnd {

    private ActivityEndObject doc;

    public ActivityEnd(String activityInstanceId, Date endDate) {
        this.doc = new ActivityEndObject(activityInstanceId, LifecycleType.ENDED, endDate);
    }

    public ActivityEndObject getDoc() {
        return doc;
    }

    public class ActivityEndObject {
        private String activityInstanceId;
        private LifecycleType lifecycleType;
        private Date endDate;

        public ActivityEndObject(String activityInstanceId, LifecycleType lifecycleType, Date endDate) {
            this.activityInstanceId = activityInstanceId;
            this.lifecycleType = lifecycleType;
            this.endDate = endDate;
        }

        public String getActivityInstanceId() {
            return activityInstanceId;
        }

        public void setActivityInstanceId(String activityInstanceId) {
            this.activityInstanceId = activityInstanceId;
        }

        public LifecycleType getLifecycleType() {
            return lifecycleType;
        }

        public void setLifecycleType(LifecycleType lifecycleType) {
            this.lifecycleType = lifecycleType;
        }

        public Date getEndDate() {
            return endDate;
        }

        public void setEndDate(Date endDate) {
            this.endDate = endDate;
        }
    }
}
