package net.pervukhin.bpmlight.exporter.elastic.repository;

import net.pervukhin.bpmlight.dto.PaginationContainerDTO;
import net.pervukhin.bpmlight.exporter.elastic.dto.ElasticResult;
import net.pervukhin.bpmlight.exporter.elastic.dto.HitsContainer;
import net.pervukhin.bpmlight.model.Activity;
import net.pervukhin.bpmlight.repository.ActivityRepositoryInt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.stream.Collectors;

@Service("activityRepository")
@ConditionalOnProperty(prefix = "exporter", name = "type", havingValue = "elastic")
public class ActivityRepository implements ActivityRepositoryInt {
    @Autowired
    private RestTemplate restTemplate;

    @Value("${rest.activity.list.byProcessInstanceId}")
    private String byProcessInstanceIdUrl;

    public PaginationContainerDTO<Activity> getList(String processInstanceId, String maxResults, String firstResults) {
        ElasticResult<Activity> result = restTemplate.exchange(
                byProcessInstanceIdUrl
                        .replace("{processInstanceId}", processInstanceId)
                        .replace("{size}", maxResults)
                        .replace("{from}", firstResults),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<ElasticResult<Activity>>() {}).getBody();
        return result != null
                ? new PaginationContainerDTO<>(
                result.getHits().getTotal().getValue(),
                result.getHits().getHits().stream()
                        .map(HitsContainer::getSource)
                        .collect(Collectors.toList()))
                : null;
    }
}
