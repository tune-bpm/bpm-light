package net.pervukhin.bpmlight.exporter.elastic.dto.aggregation;

import com.fasterxml.jackson.annotation.JsonProperty;
import net.pervukhin.bpmlight.model.ExportRecord;

public class Aggregation <T extends ExportRecord> {
    @JsonProperty("pre_filter")
    private PreFilter<T> preFilter;

    public PreFilter<T> getPreFilter() {
        return preFilter;
    }

    public void setPreFilter(PreFilter<T> preFilter) {
        this.preFilter = preFilter;
    }
}
