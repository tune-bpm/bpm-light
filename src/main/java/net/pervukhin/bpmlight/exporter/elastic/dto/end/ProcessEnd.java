package net.pervukhin.bpmlight.exporter.elastic.dto.end;

import net.pervukhin.bpmlight.model.LifecycleType;

import java.util.Date;

public class ProcessEnd {

    private ProcessEndObject doc;

    public ProcessEnd(String processInstanceId, Date endDate) {
        this.doc = new ProcessEndObject(LifecycleType.ENDED, endDate, processInstanceId);
    }

    public ProcessEndObject getDoc() {
        return doc;
    }

    public class ProcessEndObject {
        private LifecycleType lifecycleType;
        private Date endDate;
        private String processInstanceId;

        public ProcessEndObject(LifecycleType lifecycleType, Date endDate, String processInstanceId) {
            this.lifecycleType = lifecycleType;
            this.endDate = endDate;
            this.processInstanceId = processInstanceId;
        }

        public LifecycleType getLifecycleType() {
            return lifecycleType;
        }

        public Date getEndDate() {
            return endDate;
        }

        public String getProcessInstanceId() {
            return processInstanceId;
        }
    }

}
