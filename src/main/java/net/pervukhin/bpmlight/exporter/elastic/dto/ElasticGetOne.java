package net.pervukhin.bpmlight.exporter.elastic.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import net.pervukhin.bpmlight.model.ExportRecord;

public class ElasticGetOne<T extends ExportRecord> {
    @JsonProperty("_source")
    private T source;

    public T getSource() {
        return source;
    }

    public void setSource(T source) {
        this.source = source;
    }
}
