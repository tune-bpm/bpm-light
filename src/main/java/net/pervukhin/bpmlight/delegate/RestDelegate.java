package net.pervukhin.bpmlight.delegate;

import net.pervukhin.bpmlight.service.RestExecutorService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RestDelegate implements JavaDelegate {

    @Autowired
    private RestExecutorService restExecutorService;

    @Override
    public void execute(DelegateExecution execution) {
        restExecutorService.doRestRequest();
    }
}
