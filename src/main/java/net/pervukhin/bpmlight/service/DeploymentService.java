package net.pervukhin.bpmlight.service;

import net.pervukhin.bpmlight.model.Deployment;
import net.pervukhin.bpmlight.model.Model;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.impl.persistence.entity.DeploymentEntity;
import org.camunda.bpm.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;

@Service
public class DeploymentService {
    @Autowired
    private ProcessEngine processEngine;

    @Autowired
    private ExporterRegistryService exporterRegistryService;

    public void deploy(Deployment deployment) {
        BpmnModelInstance modelInstance = Bpmn.readModelFromStream(
                new ByteArrayInputStream(deployment.getXml().getBytes()));

        DeploymentEntity deployObject = (DeploymentEntity) processEngine.getRepositoryService()
                .createDeployment()
                .addModelInstance(deployment.getFileName(), modelInstance)
                .deploy();
        final Model model = new Model();
        model.setDate(deployObject.getDeploymentTime());
        model.setFileName(deployment.getFileName());
        deployment.setDate(deployObject.getDeploymentTime());

        deployObject.getDeployedArtifacts().values().forEach(item ->
            item.forEach(subItem -> {
                ProcessDefinitionEntity processDefinitionEntity = (ProcessDefinitionEntity) subItem;
                deployment.setDeploymentKey(processDefinitionEntity.getKey());
                deployment.setDeploymentId(processDefinitionEntity.getId());
                model.setDeploymentKey(processDefinitionEntity.getKey());
            })
        );
        exporterRegistryService.registerDeployment(deployment);
        exporterRegistryService.registerModel(model);
    }
}
