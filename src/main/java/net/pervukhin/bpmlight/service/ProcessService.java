package net.pervukhin.bpmlight.service;

import org.camunda.bpm.engine.ProcessEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProcessService {
    @Autowired
    private ProcessEngine processEngine;

    public void startProcess(String processId) {
        processEngine.getRuntimeService().startProcessInstanceByKey(processId);
    }

}
