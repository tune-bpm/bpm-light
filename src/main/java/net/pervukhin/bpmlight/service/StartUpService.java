package net.pervukhin.bpmlight.service;

import net.pervukhin.bpmlight.model.Deployment;
import net.pervukhin.bpmlight.util.IoUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class StartUpService {
    private static final Logger logger = LoggerFactory.getLogger(StartUpService.class);

    @Autowired
    private DeploymentService deploymentService;

    @Autowired
    private ProcessService processService;

    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        deploymentService.deploy(new Deployment(new Date(), "",
                IoUtil.getResourceFileAsString("tuneBPM.bpmn"), "tuneBPM.bpmn", ""));

        logger.info("На старт");
        load(1000);
        load(10000);
        load(20000);
        load(30000);
        load(40000);
        load(50000);
        load(60000);
        load(70000);
        load(80000);
        load(90000);
        load(100000);
    }

    private void load(int quantity) {
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < quantity; i++) {
            processService.startProcess("tuneBPM");
        }
        long endTime = System.currentTimeMillis();
        logger.info("Время выполнения|"+ quantity +"|" + (endTime-startTime));
    }
}
