package net.pervukhin.bpmlight.model;

public enum ExportRecordType {
    ACTIVITY,
    DEPLOYMENT,
    PROCESS,
    VARIABLE,
    MODEL
}
