package net.pervukhin.bpmlight.model;

public enum LifecycleType {
    STARTED,
    ENDED
}
