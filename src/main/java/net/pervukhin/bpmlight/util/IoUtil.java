package net.pervukhin.bpmlight.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class IoUtil {
    private IoUtil() {
    }

    public static String getResourceFileAsString(String fileName) {
        final InputStream is = getResourceFileAsInputStream(fileName);
        if (is != null) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            return reader.lines().collect(Collectors.joining(System.lineSeparator()));
        } else {
            throw new RuntimeException("resource not found");
        }
    }

    private static InputStream getResourceFileAsInputStream(String fileName) {
        return IoUtil.class.getClassLoader().getResourceAsStream(fileName);
    }
}
