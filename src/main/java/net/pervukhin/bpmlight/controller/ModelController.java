package net.pervukhin.bpmlight.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.pervukhin.bpmlight.model.Model;
import net.pervukhin.bpmlight.dto.PaginationContainerDTO;
import net.pervukhin.bpmlight.repository.ModelRepositoryInt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/model")
@Api(description = "Список установках процессов")
public class ModelController {
    @Autowired
    @Qualifier("modelRepository")
    private ModelRepositoryInt modelRepository;

    @GetMapping("")
    @ApiOperation("Получить список установок")
    public PaginationContainerDTO<Model> getList(
            @ApiParam("Пагинация, кол-во элементов при выдаче")
            @RequestParam(required = false, defaultValue = "10") String maxResults,

            @ApiParam("Пагинация, начинать с элемента")
            @RequestParam(required = false, defaultValue = "0") String firstResults) {

        return modelRepository.getList(maxResults, firstResults);
    }
}
